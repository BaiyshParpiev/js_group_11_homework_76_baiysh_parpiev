const fs = require('fs');
const {nanoid} = require('nanoid');

const filename = './messages.json';
let data = [];

module.exports = {
  init() {
    try {
      const fileContents = fs.readFileSync(filename);
      data = JSON.parse(fileContents);
    } catch (e) {
      data = [];
    }
  },
  getItem(datetime){
    let newArray = [];
    const index = data.indexOf(data.find(i => i.datetime === datetime));
    if(!index){
      return 'Sorry, datetime is not valid';
    }
    for(let i = 0; i < data.length; i++){
      if(index < i){
        newArray.push(data[i])
      }
    }
    return newArray;
  },
  getItems() {
    return data.slice(-30);
  },
  addItem(item) {
    item.id = nanoid();
    const date = new Date();
    item.datetime = date.toISOString();
    data.push(item);
    this.save();
    return item;
  },
  save() {
    fs.writeFileSync(filename, JSON.stringify(data));
  }
};