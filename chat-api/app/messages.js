const express = require('express');
const router = express.Router();
const fileDb = require('../fileDb');

router.get('/', (req, res) => {
  if(req.query.datetime){
    const datetime = fileDb.getItem(req.query.datetime);
    if(!Array.isArray(datetime)){
      return res.status(400).send({error: datetime});
    }
    res.send(datetime);
  }else if(!req.query.datetime){
    const messages = fileDb.getItems();
    res.send(messages);
  }

});

router.post('/', (req, res) => {
  if(!req.body.author || !req.body.message){
    return res.status(400).send({error: 'Author and message must be present in the request'});
  }
  const newMessage = fileDb.addItem({
    author: req.body.author,
    message: req.body.message,
  })
  res.send(newMessage);
});

module.exports = router
