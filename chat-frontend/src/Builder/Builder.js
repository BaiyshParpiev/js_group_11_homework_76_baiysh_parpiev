import React from 'react';
import {useEffect} from "react";
import Input from "../Components/Input/Input";
import Messages from "../Components/Messages/Messages";
import {useDispatch, useSelector} from "react-redux";
import {createMessage, fetchMessage, fetchMessages, inputChange} from "../store/actions/messagesActions";

let data = '';

const Builder = () => {
    const {messages, updateMessage, value, error} = useSelector(state => state);
    const dispatch = useDispatch();


    const onHandlerChange = e => {
        dispatch(inputChange(e.target.value))
    }

    const onSendMessage = async () => {
        const message = {
            author: "It-is-me",
            message: value,
        };
        dispatch(createMessage(message));
        dispatch(inputChange(''))
    }

    useEffect(() => {
        let interval;
        const getData = async () => {
            await dispatch(fetchMessages());
            if(messages.length > 0){
                data = messages[messages.length - 1].datetime
            }
            interval = setInterval( () => {
               if(data){
                   dispatch(fetchMessage('?datetime=' + data))
                   if (updateMessage.length > 0) {
                       data = updateMessage[updateMessage.length - 1].datetime;
                   } else {
                       return null;
                   }
                   window.scrollTo(0, document.body.scrollHeight);
               }
            }, 4000);
        }
        getData().finally(() => {console.log(messages)})
        return () => {
            clearInterval(interval)
        };
    }, [dispatch, messages, updateMessage]);


    return (
        <div className="container">
            {error && (<div style={{backgroundColor: 'red', color: 'white', padding: '15px'}}>{error}</div>)}
            <div className="listOf-messages">
                {messages && messages.map(m => (
                    <Messages key={m.id} label={m.message} class={m.author}/>
                ))}
            </div>
            <div className="send-message">
                <Input value={value} changeHandler={e => onHandlerChange(e)} click={onSendMessage}/>
            </div>
        </div>
    );
};

export default Builder;