import {
    CREATE_MESSAGE_FAILURE,
    CREATE_MESSAGE_REQUEST, CREATE_MESSAGE_SUCCESS,
    FETCH_MESSAGE_FAILURE,
    FETCH_MESSAGE_REQUEST, FETCH_MESSAGE_SUCCESS,
    FETCH_MESSAGES_FAILURE,
    FETCH_MESSAGES_REQUEST,
    FETCH_MESSAGES_SUCCESS, INPUT_CHANGE
} from "../actions/messagesActions";

const initialState = {
    fetchLoading: false,
    messages: [],
    updateMessage: [],
    value: '',
    error: null,
};

const messagesReducer = (state = initialState, action) => {
    switch(action.type){
        case INPUT_CHANGE:
            return {...state, value: action.payload};
        case FETCH_MESSAGES_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_MESSAGES_SUCCESS:
            return {...state, messages: action.payload, fetchLoading: false};
        case FETCH_MESSAGES_FAILURE:
            return {...state, fetchLoading: false, error: action.payload};
        case FETCH_MESSAGE_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_MESSAGE_SUCCESS:
            return {...state, updateMessage: action.payload, messages: [...state.messages, ...action.payload],  fetchLoading: false};
        case FETCH_MESSAGE_FAILURE:
            return {...state, fetchLoading: false, error: action.payload};
        case CREATE_MESSAGE_REQUEST:
            return {...state, fetchLoading: true};
        case CREATE_MESSAGE_SUCCESS:
            return {...state,  fetchLoading: false};
        case CREATE_MESSAGE_FAILURE:
            return {...state, fetchLoading: false, error: action.payload};
        default:
            return state;
    }
};

export default messagesReducer;