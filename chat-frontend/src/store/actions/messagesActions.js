import axios from "axios";

export const FETCH_MESSAGES_REQUEST = 'FETCH_MESSAGES_REQUEST';
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_MESSAGES_FAILURE = 'FETCH_MESSAGES_FAILURE';

export const FETCH_MESSAGE_REQUEST = 'FETCH_MESSAGE_REQUEST';
export const FETCH_MESSAGE_SUCCESS = 'FETCH_MESSAGE_SUCCESS';
export const FETCH_MESSAGE_FAILURE = 'FETCH_MESSAGE_FAILURE';

export const CREATE_MESSAGE_REQUEST = 'CREATE_MESSAGE_REQUEST';
export const CREATE_MESSAGE_SUCCESS = 'CREATE_MESSAGE_SUCCESS';
export const CREATE_MESSAGE_FAILURE = 'CREATE_MESSAGE_FAILURE';

export const INPUT_CHANGE = "INPUT_CHANGE";

export const inputChange = value => ({type: INPUT_CHANGE, payload: value});


export const fetchMessagesRequest = () => ({type: FETCH_MESSAGES_REQUEST});
export const fetchMessagesSuccess = messages => ({type: FETCH_MESSAGES_SUCCESS, payload: messages});
export const fetchMessagesFailure = error => ({type: FETCH_MESSAGES_FAILURE, payload: error});

export const fetchMessageRequest = () => ({type: FETCH_MESSAGE_REQUEST});
export const fetchMessageSuccess = messages => ({type: FETCH_MESSAGE_SUCCESS, payload: messages});
export const fetchMessageFailure = error => ({type: FETCH_MESSAGE_FAILURE, payload: error});

export const createMessageRequest = () => ({type: CREATE_MESSAGE_REQUEST});
export const createMessageSuccess = () => ({type: CREATE_MESSAGE_SUCCESS});
export const createMessageFailure = error => ({type: CREATE_MESSAGE_FAILURE, payload: error});

export const fetchMessages = () => {
    return async dispatch => {
       try{
           dispatch(fetchMessagesRequest());
           const response = await axios.get('http://localhost:8000/messages');
           dispatch(fetchMessagesSuccess(response.data))
       }catch(e){
           dispatch(fetchMessagesFailure());
       }
    }
};

export const fetchMessage = id => {
    return async dispatch => {
       try{
           dispatch(fetchMessageRequest());
           const response = await axios.get('http://localhost:8000/messages' + id);
           dispatch(fetchMessageSuccess(response.data))
       }catch(e){
           dispatch(fetchMessageFailure());
       }
    }
};

export const createMessage = message => {
    return async dispatch => {
       try{
           dispatch(createMessageRequest());
           await axios.post('http://localhost:8000/messages/', message);
           dispatch(createMessageSuccess())
       }catch(e){
           dispatch(createMessageFailure());
           throw e;
       }
    }
};