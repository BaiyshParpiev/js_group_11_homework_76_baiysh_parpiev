import React from 'react';
import './Messages.css';
const Messages = props => (
        <p key={props.id} className={props.class}><span>{props.class}</span>{props.label}</p>
    );

export default Messages;