import React from 'react';
import Button from "../Button/Button";
import './Input.css'

const Input = props => {
    return (
        <>
            <label className="form__label">Enter your Message</label>
            <input className="form__input" type="text" onChange={props.changeHandler} value={props.value}/>
            <Button type="Send" click={props.click}/>
        </>
    );
};

export default Input;